package com.gubadev.task;

import com.gubadev.task.model.Product;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Main {
    private static final PriceCombiner priceCombiner = new PriceCombiner();

    private static final Collection<Product> oldPrices = new ArrayList<>();
    private static final Collection<Product> newPrices = new ArrayList<>();

    private static final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");


    public static void main(String[] args) {
        try {
            Product productOne = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("01.01.2013 00:00:00"),
                    format.parse("31.01.2013 23:59:59"),
                    11000);
            Product productTwo = new Product(0,
                    "122856",
                    2,
                    1,
                    format.parse("10.01.2013 00:00:00"),
                    format.parse("20.01.2013 23:59:59"),
                    99000);
            Product productThree = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("01.01.2013 00:00:00"),
                    format.parse("31.01.2013 00:00:00"),
                    5000);
            oldPrices.add(productOne);
            oldPrices.add(productTwo);
            oldPrices.add(productThree);


            Product productON = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("20.01.2013 00:00:00"),
                    format.parse("20.02.2013 23:59:59"),
                    11000);

            Product productTwN = new Product(0,
                    "122856",
                    2,
                    1,
                    format.parse("15.01.2013 00:00:00"),
                    format.parse("25.01.2013 23:59:59"),
                    92000);


            Product productThreeN = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("12.01.2013 00:00:00"),
                    format.parse("13.01.2013 00:00:00"),
                    4000);

            newPrices.add(productON);
            newPrices.add(productTwN);
            newPrices.add(productThreeN);

            Collection<Product> recent = priceCombiner.getRecentPrices(oldPrices, newPrices);
            for(Product product : recent){
                System.out.println(product);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
