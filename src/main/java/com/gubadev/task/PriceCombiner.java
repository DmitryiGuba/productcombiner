package com.gubadev.task;

import com.gubadev.task.model.Product;

import java.util.Collection;
import java.util.LinkedList;

public class PriceCombiner {

    public Collection<Product> getRecentPrices(Collection<Product> oldPrices, Collection<Product> newPrices) {
        if (oldPrices == null || oldPrices.isEmpty()) return newPrices;
        if (newPrices == null || newPrices.isEmpty()) return oldPrices;

        Collection<Product> recent = new LinkedList<>();
        boolean isTheSameValue = false;
        recent.addAll(oldPrices);
        Product newPriceTemplate = null;
        for (Product newPrice : newPrices) {
            newPriceTemplate = newPrice.clone();
            for (Product oldPrice : oldPrices) {
                //check if the same productCode, number and depart
                if (newPrice.isTheSameProduct(oldPrice)) {
                    //check if prices not intersect
                    if (oldPrice.getEnd().before(newPrice.getBegin())
                            || oldPrice.getBegin().after(newPrice.getEnd())) continue;

                    //check if new price overlaps
                    if (oldPrice.getBegin().after(newPrice.getBegin())
                            && oldPrice.getEnd().before(newPrice.getEnd())) {
                        recent.remove(oldPrice);
                    }

                    //check if new price divides old price
                    if (oldPrice.getBegin().before(newPrice.getBegin())
                            && oldPrice.getEnd().after(newPrice.getEnd())) {
                        Product newPriceInterval = oldPrice.clone();
                        newPriceInterval.setBegin(newPrice.getEnd());
                        oldPrice.setEnd(newPrice.getBegin());
                        recent.add(newPriceInterval);
                    }

                    //check if old price starts before new price but intersect
                    if (oldPrice.getBegin().before(newPrice.getBegin())
                            && oldPrice.getEnd().after(newPrice.getBegin())) {
                        if(oldPrice.getValue() != newPrice.getValue()){
                            oldPrice.setEnd(newPrice.getBegin());
                        } else {
                            recent.remove(oldPrice);
                            newPriceTemplate.setBegin(oldPrice.getBegin());
                        }
                    }

                    //check if old price starts with new price but ends after
                    if (oldPrice.getBegin().before(newPrice.getEnd())
                            && oldPrice.getEnd().after(newPrice.getEnd())) {
                        if(oldPrice.getValue() != newPrice.getValue()){
                            oldPrice.setBegin(newPrice.getEnd());
                        } else {
                            recent.remove(oldPrice);
                            newPriceTemplate.setEnd(oldPrice.getEnd());
                        }
                    }

                }
            }
            recent.add(newPriceTemplate);
        }
        return recent;
    }

}
