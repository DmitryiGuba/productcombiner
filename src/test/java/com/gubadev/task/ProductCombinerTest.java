package com.gubadev.task;

import com.gubadev.task.model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ProductCombinerTest {
    private final PriceCombiner priceCombiner = new PriceCombiner();

    private final Collection<Product> oldPrices = new ArrayList<>();

    private final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    @Before
    public void createOldPrices() {
        try {
            Product productOne = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("01.01.2013 00:00:00"),
                    format.parse("31.01.2013 23:59:59"),
                    11000);
            Product productTwo = new Product(0,
                    "122856",
                    2,
                    1,
                    format.parse("10.01.2013 00:00:00"),
                    format.parse("20.01.2013 23:59:59"),
                    99000);
            Product productThree = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("01.01.2013 00:00:00"),
                    format.parse("31.01.2013 00:00:00"),
                    5000);
            oldPrices.add(productOne);
            oldPrices.add(productTwo);
            oldPrices.add(productThree);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @After
    public void clearOldPrices() {
        oldPrices.clear();
    }

    @Test
    public void oldBeforeNewAndNotIntersect() {
        try {
            Product product = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("20.02.2013 00:00:00"),
                    format.parse("20.03.2013 23:59:59"),
                    11000);
            Collection<Product> newPrices = new ArrayList<>();
            newPrices.add(product);
            Collection<Product> recent = priceCombiner.getRecentPrices(oldPrices, newPrices);

            Product expectedOldProducr = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("01.01.2013 00:00:00"),
                    format.parse("31.01.2013 23:59:59"),
                    11000);

            assertEquals(4, recent.size());
            assertTrue(recent.contains(product));
            assertTrue(recent.contains(expectedOldProducr));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void oldAfterNewAndIntersect() {
        try {
            Product product = new Product(0,
                    "122856",
                    2,
                    1,
                    format.parse("15.01.2013 00:00:00"),
                    format.parse("25.01.2013 23:59:59"),
                    92000);

            Collection<Product> newPrices = new ArrayList<>();
            newPrices.add(product);

            Collection<Product> recent = priceCombiner.getRecentPrices(oldPrices, newPrices);

            Product expectedProduct = new Product(0,
                    "122856",
                    2,
                    1,
                    format.parse("10.01.2013 00:00:00"),
                    format.parse("15.01.2013 00:00:00"),
                    99000);

            assertEquals(4, recent.size());
            assertTrue(recent.contains(product));
            assertTrue(recent.contains(expectedProduct));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void oldAfterNewAndNotIntersect() {
        try {
            Product product = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("01.02.2013 00:00:00"),
                    format.parse("20.02.2013 23:59:59"),
                    11000);
            Collection<Product> newPrices = new ArrayList<>();
            newPrices.add(product);

            Product expectedOldProducr = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("21.02.2013 00:00:00"),
                    format.parse("31.02.2013 23:59:59"),
                    11000);

            oldPrices.add(expectedOldProducr);
            Collection<Product> recent = priceCombiner.getRecentPrices(oldPrices, newPrices);

            assertEquals(5, recent.size());
            assertTrue(recent.contains(product));
            assertTrue(recent.contains(expectedOldProducr));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void oldAfterNewAndIntersectWithSamePrice() {
        try {
            Product product = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("20.01.2013 00:00:00"),
                    format.parse("20.02.2013 23:59:59"),
                    11000);
            Collection<Product> newPrices = new ArrayList<>();
            newPrices.add(product);
            Collection<Product> recent = priceCombiner.getRecentPrices(oldPrices, newPrices);


            Product expectedOldProducr = new Product(0,
                    "122856",
                    1,
                    1,
                    format.parse("01.01.2013 00:00:00"),
                    format.parse("20.02.2013 23:59:59"),
                    11000);


            assertEquals(3, recent.size());
            assertTrue(recent.contains(expectedOldProducr));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void oldDividedByNew() {
        try {
            Product product = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("12.01.2013 00:00:00"),
                    format.parse("13.01.2013 00:00:00"),
                    4000);

            Collection<Product> newPrices = new ArrayList<>();
            newPrices.add(product);


            Collection<Product> recent = priceCombiner.getRecentPrices(oldPrices, newPrices);


            Product firstHalfFromOld = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("01.01.2013 00:00:00"),
                    format.parse("12.01.2013 00:00:00"),
                    5000);
            Product secondHalfFromOld = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("13.01.2013 00:00:00"),
                    format.parse("31.01.2013 00:00:00"),
                    5000);


            assertEquals(5, recent.size());
            assertTrue(recent.contains(product));
            assertTrue(recent.contains(firstHalfFromOld));
            assertTrue(recent.contains(secondHalfFromOld));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void newOverlapsOld() {
        try {
            Product product = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("12.12.2012 00:00:00"),
                    format.parse("13.02.2013 00:00:00"),
                    4000);

            Collection<Product> newPrices = new ArrayList<>();
            newPrices.add(product);


            Collection<Product> recent = priceCombiner.getRecentPrices(oldPrices, newPrices);

            assertEquals(3, recent.size());
            assertTrue(recent.contains(product));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void newOvelapsOldChain() {
        try {
            oldPrices.add(new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("31.01.2013 00:00:00"),
                    format.parse("31.02.2013 00:00:00"),
                    6000));
            oldPrices.add(new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("31.02.2013 00:00:00"),
                    format.parse("31.03.2013 00:00:00"),
                    7000));
            oldPrices.add(new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("31.03.2013 00:00:00"),
                    format.parse("31.04.2013 00:00:00"),
                    8000));
            oldPrices.add(new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("31.04.2013 00:00:00"),
                    format.parse("31.05.2013 00:00:00"),
                    9000));

            Product product = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("21.01.2013 00:00:00"),
                    format.parse("21.05.2013 00:00:00"),
                    9000);
            Product expectedProduct = new Product(0,
                    "6654",
                    1,
                    2,
                    format.parse("21.01.2013 00:00:00"),
                    format.parse("31.05.2013 00:00:00"),
                    9000);
            Collection<Product> newPrices = new ArrayList<>();
            newPrices.add(product);
            Collection<Product> recent = priceCombiner.getRecentPrices(oldPrices, newPrices);
            assertEquals(4, recent.size());
            assertTrue(recent.contains(expectedProduct));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
